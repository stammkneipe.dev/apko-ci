# Apko CI

## Getting started

This docker image will help you build docker images with APKO in a CI CD Environment.

> registry.gitlab.com/stammkneipe-dev/apko-ci:latest

## Example Pipeline

```yaml
containerize-package:
  image: registry.gitlab.com/stammkneipe-dev/apko-ci:latest
  variables:
    APKO_FILE: "apko.yml"
    FULL_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA-$CI_PIPELINE_IID
  before_script:
    - apko version
    - apko login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - apko publish "${APKO_FILE}" "${FULL_IMAGE_NAME}"
```

## Contributing

[Contributing](/CONTRIBUTING.md)

## License

[MIT License](/LICENSE)

## Authors and acknowledgment

- [APKO](https://github.com/chainguard-dev/apko)
- [Patrick Domnick](https://gitlab.com/PatrickDomnick)
- [Henry Sachs](https://gitlab.com/DerAstronaut)
